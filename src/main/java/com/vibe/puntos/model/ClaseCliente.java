package com.vibe.puntos.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@SuppressWarnings("serial")
@XmlRootElement
public class ClaseCliente implements Serializable {
	Integer nroClase;
	String nombreClase;


	public ClaseCliente() {
		
	}


	public Integer getNroClase() {
		return nroClase;
	}


	public void setNroClase(Integer nroClase) {
		this.nroClase = nroClase;
	}


	public String getNombreClase() {
		return nombreClase;
	}


	public void setNombreClase(String nombreClase) {
		this.nombreClase = nombreClase;
	}
	
	public ClaseCliente(Integer nro, String nombre) {
		this.nroClase = nro;
		this.nombreClase = nombre;
	}
	
	
}
