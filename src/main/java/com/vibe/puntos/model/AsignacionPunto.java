package com.vibe.puntos.model;

public class AsignacionPunto {

    private int id;
    private int limiteInferior;
    private int limiteSuperior;
    private int montoEquivalente;

    public AsignacionPunto(int id, int limiteInferior, int limiteSuperior, int montoEquivalente) {
        this.id = id;
        this.limiteInferior = limiteInferior;
        this.limiteSuperior = limiteSuperior;
        this.montoEquivalente = montoEquivalente;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLimiteInferior() {
        return limiteInferior;
    }

    public void setLimiteInferior(int limiteInferior) {
        this.limiteInferior = limiteInferior;
    }

    public int getLimiteSuperior() {
        return limiteSuperior;
    }

    public void setLimiteSuperior(int limiteSuperior) {
        this.limiteSuperior = limiteSuperior;
    }

    public int getMontoEquivalente() {
        return montoEquivalente;
    }

    public void setMontoEquivalente(int montoEquivalente) {
        this.montoEquivalente = montoEquivalente;
    }
}