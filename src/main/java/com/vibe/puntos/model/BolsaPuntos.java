package com.vibe.puntos.model;

import java.util.Date;

public class BolsaPuntos {

    private int id;
    private Cliente cliente;
    private Date fechaAsignacion;
    private Date fechaCaducidad;
    private int puntosAsignados;
    private int puntosUtilizados;
    private int saldo;
    private int monto;
    
    public BolsaPuntos() {}
    
    
	public BolsaPuntos(int id, Cliente cliente, Date fechaAsignacion, Date fechaCaducidad, int puntosAsignados,
			int puntosUtilizados, int saldo, int monto) {
		super();
		this.id = id;
		this.cliente = cliente;
		this.fechaAsignacion = fechaAsignacion;
		this.fechaCaducidad = fechaCaducidad;
		this.puntosAsignados = puntosAsignados;
		this.puntosUtilizados = puntosUtilizados;
		this.saldo = saldo;
		this.monto = monto;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Date getFechaAsignacion() {
		return fechaAsignacion;
	}
	public void setFechaAsignacion(Date fechaAsignacion) {
		this.fechaAsignacion = fechaAsignacion;
	}
	public Date getFechaCaducidad() {
		return fechaCaducidad;
	}
	public void setFechaCaducidad(Date fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}
	public int getPuntosAsignados() {
		return puntosAsignados;
	}
	public void setPuntosAsignados(int puntosAsignados) {
		this.puntosAsignados = puntosAsignados;
	}
	public int getPuntosUtilizados() {
		return puntosUtilizados;
	}
	public void setPuntosUtilizados(int puntosUtilizados) {
		this.puntosUtilizados = puntosUtilizados;
	}
	public int getSaldo() {
		return saldo;
	}
	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}
	public int getMonto() {
		return monto;
	}
	public void setMonto(int monto) {
		this.monto = monto;
	}
    
    

   
}