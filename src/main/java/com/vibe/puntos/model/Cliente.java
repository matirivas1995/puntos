package com.vibe.puntos.model;

import java.sql.Date;

public class Cliente {

    private int id;
    private int nombre;
    private int ci;
    private String tipoDocumento;
    private String nacionalidad;
    private String email;
    private String telefono;
    private Date nacimiento;
    private ClaseCliente clase;
    private Date asignacionDeClase;
    
    
    public Cliente() {}
    
    
	public Cliente(int id, int nombre, int ci, String tipoDocumento, String nacionalidad, String email, String telefono,
			Date nacimiento, ClaseCliente clase, Date asignacionDeClase) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.ci = ci;
		this.tipoDocumento = tipoDocumento;
		this.nacionalidad = nacionalidad;
		this.email = email;
		this.telefono = telefono;
		this.nacimiento = nacimiento;
		this.clase = clase;
		this.asignacionDeClase = asignacionDeClase;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNombre() {
		return nombre;
	}
	public void setNombre(int nombre) {
		this.nombre = nombre;
	}
	public int getCi() {
		return ci;
	}
	public void setCi(int ci) {
		this.ci = ci;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getNacionalidad() {
		return nacionalidad;
	}
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public Date getNacimiento() {
		return nacimiento;
	}
	public void setNacimiento(Date nacimiento) {
		this.nacimiento = nacimiento;
	}
	public ClaseCliente getClase() {
		return clase;
	}
	public void setClase(ClaseCliente clase) {
		this.clase = clase;
	}
	public Date getAsignacionDeClase() {
		return asignacionDeClase;
	}
	public void setAsignacionDeClase(Date asignacionDeClase) {
		this.asignacionDeClase = asignacionDeClase;
	}
    
    

   
}