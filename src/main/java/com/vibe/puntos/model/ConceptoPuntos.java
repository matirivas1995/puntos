package com.vibe.puntos.model;

public class ConceptoPuntos {

    private int id;
    private String descripcion;
    private int requeridos;
    
    public ConceptoPuntos() {}

    public ConceptoPuntos(int id, String descripcion, int requeridos) {
        this.id = id;
        this.descripcion = descripcion;
        this.requeridos = requeridos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getRequeridos() {
		return requeridos;
	}

	public void setRequeridos(int requeridos) {
		this.requeridos = requeridos;
	}

   
}