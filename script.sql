-- CLASES DE ELIAS
CREATE TABLE clase_cliente(
nroclase integer PRIMARY KEY,
nombreclase varchar(30)
);

CREATE SEQUENCE cliente_id_seq;


CREATE TABLE cliente(
id integer DEFAULT nextval('cliente_id_seq') PRIMARY KEY,
nombre varchar(30) NOT NULL,
apellido varchar(30) NOT NULL,
nrodocumento integer UNIQUE,
tipodocumento char(1),
nacionalidad varchar(30),
email varchar(40),
telefono varchar(20),
fechanacimiento date,
clase integer,
fasignclase date,
CONSTRAINT chk_tipodoc CHECK(tipodocumento in('C','P')),
CONSTRAINT FKC FOREIGN KEY(clase) REFERENCES clase_cliente(nroclase)
);

ALTER SEQUENCE cliente_id_seq OWNED BY cliente.id;

-- CLASES DE MATI

CREATE SEQUENCE concepto_id_seq;
CREATE TABLE concepto (
    id integer NOT NULL DEFAULT nextval('concepto_id_seq') PRIMARY KEY,
    descripcion varchar(30) NOT NULL,
    puntos_requeridos integer NOT NULL
);


CREATE SEQUENCE asignacion_id_seq;
CREATE TABLE asignacion (
    id integer NOT NULL DEFAULT nextval('asignacion_id_seq') PRIMARY KEY,
    inferior integer NOT NULL,
    superior integer NOT NULL,
    equivalencia integer NOT NULL
);

CREATE SEQUENCE bolsa_puntos_id_seq;
CREATE TABLE bolsa_puntos (
    id integer NOT NULL DEFAULT nextval('bolsa_puntos_id_seq') PRIMARY KEY,
    id_cliente integer NOT NULL,
    asignacion date,
    caducidad date,
    puntos_asignados integer,
    puntos_utilizados integer,
    saldo integer,
    monto_op integer
);
-- CLASES DE SANTI


CREATE SEQUENCE seq_parametrizacion;


CREATE TABLE parametrizacion(
id integer DEFAULT nextval('seq_parametrizacion') PRIMARY KEY,
fecha_inicio date,
fecha_final date,
dias_duracion integer
);

ALTER SEQUENCE seq_parametrizacion OWNED BY parametrizacion.id;


-- CLASES DE LUIS


CREATE SEQUENCE cabecera_id_seq;
CREATE TABLE cabecera (
    id integer NOT NULL DEFAULT nextval('cabecera_id_seq') PRIMARY KEY,
    descripcion varchar(30) NOT NULL,
    puntos_utilizados integer NOT NULL,
    fecha date,
    id_concepto integer,
    CONSTRAINT FKC FOREIGN KEY(id_concepto) REFERENCES concepto(id)

);

CREATE SEQUENCE detalle_id_seq;
CREATE TABLE detalle (
    id integer NOT NULL DEFAULT nextval('detalle_id_seq') PRIMARY KEY,
    id_cabecera integer NOT NULL,
    puntos_utilizados integer NOT NULL,
    id_bolsa_puntos integer,
    uso_concepto integer,
    CONSTRAINT FKCC FOREIGN KEY(id_cabecera) REFERENCES cabecera(id),
    CONSTRAINT FKCB FOREIGN KEY(id_bolsa_puntos) REFERENCES bolsa_puntos(id)

);

CREATE TABLE uso_puntos (
    id_cabecera integer NOT NULL PRIMARY KEY,
    id_detalle integer NOT NULL,

    CONSTRAINT FKC1 FOREIGN KEY(id_cabecera) REFERENCES cabecera(id),
    CONSTRAINT FKC2 FOREIGN KEY(id_detalle) REFERENCES detalle(id)

);